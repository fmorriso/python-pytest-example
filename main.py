import sys
import pytest


def get_python_version() -> str:
    return f'{sys.version_info.major}.{sys.version_info.minor}.{sys.version_info.micro}'


def main() -> None:
    print(f'Python version: {get_python_version()}')
    print(f'Pytest version: {pytest.__version__}')


if __name__ == '__main__':
    main()
