import typing
from numbers import Number


def add(number_one: Number | str, number_two: Number | str) -> typing.Any:
    return number_one + number_two


def divide(number_one: Number, number_two: Number) -> Number:
    if number_two == 0:
        raise ValueError
    return number_one / number_two

