from abc import ABC, abstractmethod
from dataclasses import dataclass
import math
from numbers import Number


@dataclass
class Shape(ABC):

    @abstractmethod
    def area(self) -> Number:
        pass

    @abstractmethod
    def perimeter(self) -> Number:
        pass


@dataclass
class Circle(Shape):
    radius: float

    def area(self) -> Number:
        return math.pi * self.radius ** 2

    def perimeter(self) -> Number:
        return 2 * math.pi * self.radius


@dataclass
class Rectangle(Shape):
    length: Number
    width: Number

    def area(self) -> Number:
        return self.length * self.width

    def perimeter(self) -> Number:
        return 2 * (self.length + self.width)

    def __eq__(self, other):
        if not isinstance(other, type(self)):
            return False
        return self.length == other.length and self.width == other.width
