import pytest

from source import shapes as shapes


@pytest.fixture
def rectangle_first():
    return shapes.Rectangle(10, 20)


@pytest.fixture
def rectangle_second():
    return shapes.Rectangle(5, 6)