from numbers import Number

from source import shapes as shapes
import math


class TestCircle:

    def setup_method(self, method):
        print(f'Setting up {method.__name__}')
        self.circle = shapes.Circle(10)

    def teardown_method(self, method):
        print(f'Tearing down {method.__name__}')
        del self.circle

    def test_area(self):
        assert self.circle.area() == math.pi * self.circle.radius ** 2

    def test_perimeter(self):
        result: Number = self.circle.perimeter()
        expected: Number = 2 * math.pi * self.circle.radius
        assert result == expected

    def test_not_same_area(self, rectangle_first):
        assert self.circle.area() != rectangle_first.area()