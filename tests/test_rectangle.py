from numbers import Number


def test_area(rectangle_first):
    result: Number = rectangle_first.area()
    expected: Number = rectangle_first.length * rectangle_first.width
    assert result == expected


def test_perimeter(rectangle_first):
    result: Number = rectangle_first.perimeter()
    expected: Number = 2 * (rectangle_first.length + rectangle_first.width)
    assert result == expected


def test_not_equal(rectangle_first, rectangle_second):
    assert rectangle_first != rectangle_second
